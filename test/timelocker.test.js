const HRE = require('hardhat');
const chai = require("chai");

const ADMIN_WALLET = "0x69Ba7E86bbB074Cd5f72693DEb6ADc508D83A6bF";
const panCakeV2RouterAddress = "0xD99D1c33F9fC3444f8101754aBC46c52416550D1";

const DECIMAL_ZEROS = "000000000000000000"; // 18 zeros


describe("SeaToken with tax tests", function() {
    beforeEach(async function () {
        this.users = await ethers.getSigners();
        const tenEther = ethers.utils.parseEther('50');
        const SeaToken = await ethers.getContractFactory("SeaToken");
        const TokenLocker = await ethers.getContractFactory("TokenLocker");
        this.tokenLocker = await TokenLocker.deploy();
        this.seaToken = await SeaToken.deploy();
        this.panCakeRouter = await ethers.getContractAt("IPancakeV2Router02", panCakeV2RouterAddress);

        await this.seaToken.deployed();
        await this.tokenLocker.deployed();

        await this.users[0].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
        await HRE.network.provider.request({method: 'hardhat_impersonateAccount', params: [ADMIN_WALLET]})
        this.admin = await ethers.provider.getSigner(ADMIN_WALLET);

        await this.seaToken.connect(this.admin).transfer(this.users[0].address, '10000' + DECIMAL_ZEROS);
        await this.seaToken.connect(this.admin).approve(panCakeV2RouterAddress, '40000000' + DECIMAL_ZEROS); // 40M to pancake router

        await this.panCakeRouter.connect(this.admin).addLiquidityETH(this.seaToken.address, '40000000' + DECIMAL_ZEROS, 0, 0, ADMIN_WALLET, new Date().getTime(), {
            value: ethers.utils.parseEther('40')
        }); // provide 40 BNB + 40M token liquidity to pancakeswap

        await this.seaToken.connect(this.admin).burn();
    });

    it("Should lock some tokens and not allow unlock until appropriate time passes", async function () {

        const amount = `10000${DECIMAL_ZEROS}`;
        const unlocksAt = Math.round(new Date().getTime() / 1000) + 86400; // +1 Day
        await this.seaToken.connect(this.admin).approve(this.tokenLocker.address, amount); // 10000 Tokens
        await this.tokenLocker.connect(this.admin).lock(this.seaToken.address, amount, unlocksAt);

        chai.expect((await this.tokenLocker.getLocker(0))['amount'].toString()).to.be.equal(amount);

        await network.provider.send('evm_increaseTime', [0.5*86400]) // Increase Half day

        await chai.expect(
            this.tokenLocker.connect(this.admin).unlock(0)
        ).to.be.revertedWith('TL: Not ready to unlock yet')

        await network.provider.send('evm_increaseTime', [0.5*86400]) // Increase Half day

        await chai.expect(
            this.tokenLocker.connect(this.admin).unlock(0)
        ).to.be.revertedWith('TL: Not ready to unlock yet')

        await network.provider.send('evm_increaseTime', [0.5*86400]) // Increase Half day

        const balanceBefore = await this.seaToken.balanceOf(ADMIN_WALLET);
        await this.tokenLocker.connect(this.admin).unlock(0);
        const balanceAfter = await this.seaToken.balanceOf(ADMIN_WALLET);

        chai.expect(balanceAfter.sub(balanceBefore).toString()).to.be.equal('9579989500000000000000'); // after tax

        await chai.expect(
            this.tokenLocker.connect(this.admin).unlock(0)
        ).to.be.revertedWith('TL: already unlocked')
    });
});
