const HRE = require('hardhat');
require("chai");
require("bignumber.js");
const ADMIN_WALLET = "0x69Ba7E86bbB074Cd5f72693DEb6ADc508D83A6bF";
const panCakeV2RouterAddress = "0xD99D1c33F9fC3444f8101754aBC46c52416550D1";
const WETH_ADDRESS = "0xae13d989dac2f0debff460ac112a837c89baa7cd";
const FACTORY_ADDRESS = "0x6725f303b657a9451d8ba641348b6761a6cc7a17";
const DECIMAL_ZEROS = "000000000000000000"; // 18 zeros



describe("SeaToken Scenerio", function() {
    beforeEach(async function() {
        this.timeout(50000);
        this.users = await ethers.getSigners();
        const tenEther = ethers.utils.parseEther('9000');
        const SeaToken = await ethers.getContractFactory("SeaToken");
        this.seaToken = await SeaToken.deploy();
        this.panCakeRouter = await ethers.getContractAt("IPancakeV2Router02", panCakeV2RouterAddress);
        this.panCakeFactory = await ethers.getContractAt("IPancakeV2Factory", FACTORY_ADDRESS);

        await this.seaToken.deployed();

        const pairAddress = await this.panCakeFactory.getPair(WETH_ADDRESS, this.seaToken.address);

        this.panCakePair = await ethers.getContractAt("IPancakeV2Pair", pairAddress);

        await this.users[0].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
        await this.users[1].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
        await this.users[2].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
        await HRE.network.provider.request({method: 'hardhat_impersonateAccount', params: [ADMIN_WALLET]})

        this.admin = await ethers.provider.getSigner(ADMIN_WALLET);

        await this.seaToken.connect(this.admin).transfer(this.users[0].address, '10000' + DECIMAL_ZEROS);
        await this.seaToken.connect(this.admin).approve(panCakeV2RouterAddress, '40000000' + DECIMAL_ZEROS); // 40M to pancake router

        await this.panCakeRouter.connect(this.admin).addLiquidityETH(this.seaToken.address, '100000' + DECIMAL_ZEROS, 0, 0, ADMIN_WALLET, new Date().getTime(), {
            value: ethers.utils.parseEther('1000')
        }); // provide 1000 BNB + 100000 token liquidity to pancakeswap

        await this.seaToken.connect(this.admin).burn();
    });

    it("It's a scenerio of when user buy 100 tokens and sell 100 tokens", async function() {
        let seaPrice, wethReserve, seaReserve;
        const path = [this.seaToken.address, WETH_ADDRESS];
        console.log("Initial price and liquidity");
        let reserves = await this.panCakePair.getReserves();
        seaReserve = bigNumberToHumanSensibleNumber(reserves['reserve0']);
        wethReserve = bigNumberToHumanSensibleNumber(reserves['reserve1']);
        seaPrice = bigNumberToHumanSensibleNumber((await this.panCakeRouter.getAmountsOut(ethers.utils.parseEther('1'), path))[1]);
        printTable(seaReserve, wethReserve, seaPrice);
        console.log("After sale price and liquidity");
        await this.seaToken.connect(this.users[0]).approve(panCakeV2RouterAddress, '100' + DECIMAL_ZEROS);
        await this.panCakeRouter.connect(this.users[0]).swapExactTokensForETHSupportingFeeOnTransferTokens(
            '100' + DECIMAL_ZEROS,
            0, // accept any amount of ETH
            path,
            ADMIN_WALLET,
            new Date().getTime()
        )
        reserves = await this.panCakePair.getReserves();
        seaReserve = bigNumberToHumanSensibleNumber(reserves['reserve0']);
        wethReserve = bigNumberToHumanSensibleNumber(reserves['reserve1']);
        seaPrice = bigNumberToHumanSensibleNumber((await this.panCakeRouter.getAmountsOut(ethers.utils.parseEther('1'), path))[1]);
        printTable(seaReserve, wethReserve, seaPrice);
        console.log("After buy price and liquidity");
        await this.panCakeRouter.connect(this.users[0]).swapExactETHForTokensSupportingFeeOnTransferTokens(
            0, // accept any amount of Tokens
            path.reverse(),
            ADMIN_WALLET,
            new Date().getTime(), {
                value: ethers.utils.parseEther((parseFloat(seaPrice)*100).toString())
            }
        )
        reserves = await this.panCakePair.getReserves();
        seaReserve = bigNumberToHumanSensibleNumber(reserves['reserve0']);
        wethReserve = bigNumberToHumanSensibleNumber(reserves['reserve1']);
        seaPrice = bigNumberToHumanSensibleNumber((await this.panCakeRouter.getAmountsOut(ethers.utils.parseEther('1'), path.reverse()))[1]);
        printTable(seaReserve, wethReserve, seaPrice);
    });

    function bigNumberToHumanSensibleNumber(bigNumber) {
        const DIVISOR = 10 ** 6;
        return (parseInt(bigNumber.div(ethers.utils.parseEther('1').div(DIVISOR)).toString())/DIVISOR).toFixed(6);
    }

    function printTable(seaReserve, wethReserve, price) {
        console.table([
            ["LP Sea Amount", "LP WBNB Amount", "Price"],
            [seaReserve, wethReserve, `${price} BNB/SEA`]
        ])
    }

});
