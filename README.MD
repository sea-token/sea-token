# Sea Token
* Solidity v0.8.0
* Hardhat v2.1.2

## Development

create `.env` file in root and add proper variables from `.env.example` before running tests and coverage,   
since this contract uses liquidity locking hence to test it, it forks testnet of BSC

### Compile
`npm run compile`

### Test
`npm run test`

### Coverage
`npm run coverage`

| File                     |  % Stmts | % Branch |  % Funcs |  % Lines |
|--------------------------|----------|----------|----------|----------|
| contracts/               |      100 |    91.67 |      100 |      100 | 
| SeaToken.sol             |      100 |    91.67 |      100 |      100 |

### Tx Costs

* Solc version: 0.8.0
* Optimizer enabled: false
* Runs: 200
* Block limit: 9500000 gas
  

* Gas Fee: 20 gwei/gas
* BNB Cost: 334.39 usd/bnb


|  Contract  |  Method    |  Min         |  Max        |  Avg        |  # calls     |  usd (avg)  
|------------|------------|--------------|-------------|-------------|--------------|-------------
|  SeaToken  |  approve   |           -  |          -  |      44851  |           3  |       0.30  
|  SeaToken  |  burn      |           -  |          -  |      83797  |           5  |       0.55   
|  SeaToken  |  transfer  |       61189  |     519537  |     213972  |           6  |       1.43  
| Deployments|            |              |             |             |  % of limit  |             
|  SeaToken  |            |     4925636  |    4925646  |    4925643  |      51.8 %  |      32.66  


### Deploy
`npx hardhat run --network <network> scripts/seatoken-deploy.script.js`

### Testnet Deployments
* [0x7F27b06a4188AE0A2280AB10dEb7c16b56045303](https://testnet.bscscan.com/address/0x7F27b06a4188AE0A2280AB10dEb7c16b56045303)
* [SeaToken OneClickDapp](https://oneclickdapp.com/bison-prism)

### Mainnet Deployment
* [0xFB52FC1f90Dd2B070B9Cf7ad68ac3d68905643fa](https://bscscan.com/address/0xFB52FC1f90Dd2B070B9Cf7ad68ac3d68905643fa)

## Basic spec
* Token Name: Sea Token		
* Symbol: SEA
* Initial Supply: 200,000,000
* Decimals(1-18): 18  
*Can burn, cannot mint*

## Token outline
SEA Token is a deflationary cryptocurrency on Binance Smart Chain, with tokenomics deisgned to support organisations fighting ocean degradation.

## Transaction tax
Every transaction, 5% tax is taken, split into:
* 1% locked into Pancakeswap liquidity pool via Cryptex
* 2% distributed to existing holders, proportional to their holdings
* 2% transferred to charity wallet address, for donation to our partners (Sea Shepherd, Coral Reef Alliance, 5 Gyres, Gili Eco Trust, GreenWave, FishAct and others).

## Token breakdown
* Total supply: 200,000,000
* Liquidity on Pancakeswap (SEA): 40,000,000
* Liquidity on Pancakeswap (BNB): TBC
* Resulting in initial SEA price: TBC
* Send to burn address: 80,000,000
* Retained in main wallet: 80,000,000


# Token Locker


### Deploy
`npx hardhat run --network <network> scripts/token-locker-deploy.script.js`

### Mainnet Deployment
* [0xee17475278249f09f3861021EAE27eb6839B0004](https://bscscan.com/address/0xee17475278249f09f3861021EAE27eb6839B0004)

# License
(C)copyright janruls1 <janruls1@gmail.com>
